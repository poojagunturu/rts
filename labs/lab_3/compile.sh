echo "compiling to .o"

# compile into .o
arm-none-eabi-gcc -c -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/STM32F4xx_HAL_Driver/Inc/Legacy -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -IMiddlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/ -IMiddlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/ -IMiddlewares/Third_Party/FreeRTOS/Source/include/ -Os -ffunction-sections -fdata-sections -g -fstack-usage -Wall -specs=nano.specs Drivers/STM32F4xx_HAL_Driver/Src/*.c Src/*.c startup/startup_stm32f446xx.s Middlewares/Third_Party/FreeRTOS/Source/*.c Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/*.c Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/*.c Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/*.c
# Tried it without the D__weak and D_packed first because it was raising a syntax error
# compiled correctly, took FOREVER. a very long time indeed. Will need to be improved.
# maybe add a makefile this weekend.


echo "compiling into .elf"
# compile into .elf
arm-none-eabi-gcc -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -TSTM32F446RE_FLASH.ld -specs=nosys.specs -static -Wl,-Map=lab_1.map -Wl,--gc-sections -Wl,--defsym=malloc_getpagesize_P=0x80 -Wl,--start-group -lc -lm -Wl,--end-group -specs=nano.specs *.o -o main.elf
# this seems to work

echo "copying to .hex"
# move .elf to .hex
arm-none-eabi-objcopy -Oihex main.elf main.hex

echo "removing artifacts"
rm *.o *.su

# program it with openocd
# openocd -f /usr/shar/openocd/scripts/board/st_nucleo_f4.cfg

# open in telnet:
# telnet localhost 4444
 # reset halt
 # flash write_image erase main.hex
 # reset run

